package com.radiumone.sdk;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.radiumone.geofence_sdk.R1GeofenceListener;
import com.radiumone.geofence_sdk.geofence.R1Geofence;
import com.radiumone.sdk.testemitter.EmitterTestStartActivity;
import com.radiumone.sdk.testgeofence.R1Geofencing;
import com.radiumone.sdk.testpush.R1PushTest;
import com.radiumone.sdk.testpush.ShowNotificationActivity;


public class MainActivity extends Activity implements View.OnClickListener{

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity_layout);
		findViewById(R.id.start_emitter_test).setOnClickListener(this);
		findViewById(R.id.start_push_test).setOnClickListener(this);
		findViewById(R.id.start_geofence_test).setOnClickListener(this);

        checkDeepLink(getIntent());
	}

    @Override
	public void onClick(View v) {
		int id = v.getId();
		Intent intent = new Intent();
		if (id == R.id.start_push_test ){
			intent.setClass(this, R1PushTest.class);
		} else if ( id == R.id.start_emitter_test ){
			intent.setClass(this, EmitterTestStartActivity.class);
		} else if(id == R.id.start_geofence_test){
			intent.setClass(this, R1Geofencing.class);
		}
		startActivity(intent);
	}

    @Override
    protected void onNewIntent(Intent intent) {
        checkDeepLink(intent);
    }

    private void checkDeepLink(Intent newIntent) {

        Uri uri = newIntent.getData();
        if ( uri != null ){
            String scheme = uri.getScheme();
            if ( scheme != null ){
                if ( scheme.equals("r1connect")){

                    String host = uri.getHost();
                    if ( host != null && host.equals("options")) {
                        String path = uri.getPath();
                        if (path != null) {
                            Intent intent = new Intent();
                            if (path.equals("/shared")) {
                                intent.setClass(this, ShowNotificationActivity.class);
                                Bundle bundle = newIntent.getExtras();
                                if ( bundle != null ) {
                                    intent.putExtras(bundle);
                                }
                            } else if (path.equals("/emitter")) {
                                intent.setClass(this, EmitterTestStartActivity.class);
                            } else if (path.equals("/push")) {
                                intent.setClass(this, R1PushTest.class);
                            } else if (path.equals("/geofencing")) {
                                intent.setClass(this, R1Geofencing.class);
                            } else {
                                intent.setClass(this, MainActivity.class);
                            }
                            startActivity(intent);
                        }
                    }
                }
            }
        }
    }
}