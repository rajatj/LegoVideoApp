package com.radiumone.sdk.testpush;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.radiumone.emitter.R1Emitter;
import com.radiumone.emitter.notification.R1PushNotificationManager;
import com.radiumone.emitter.richpush.activity.R1RichPushActivity;
import com.radiumone.sdk.R;

import java.util.Set;

public class ShowNotificationActivity extends Activity {

    private boolean newData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.r1_push_notification_layout);
        newData = true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        newData = true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        R1Emitter.getInstance().onStart(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showData();
    }

    private void showData() {
        if ( newData ) {
            newData = false;
            // close all notifications for this application (optional)
            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancelAll();

            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.notifications);
                Set<String> keys = extras.keySet();
                for (String key : keys) {
                    TextView textView = new TextView(this);
                    textView.setText(key + "=" + extras.get(key));
                    linearLayout.addView(textView);
                }
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        R1Emitter.getInstance().onStop(this);
    }
}
