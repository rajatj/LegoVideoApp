package com.radiumone.sdk.testpush;

import android.app.Application;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Toast;

import com.radiumone.emitter.R1Emitter;
import com.radiumone.emitter.push.R1CommandListener;
import com.radiumone.emitter.richpush.R1Command;
import com.radiumone.emitter.richpush.activity.R1RichPushActivity;
import com.radiumone.sdk.R;

import java.util.HashMap;

public class R1sdkApplication extends Application implements R1CommandListener{

    @Override
    public void onCreate() {
        super.onCreate();
        // drawable in notification bar
        R1Emitter.getInstance().setNotificationIconResourceId(this, R.drawable.notification_icon);

        // custom BroadcastReceiver for push
        R1Emitter.getInstance().setIntentReceiver(this, TestPushReceiver.class);

        // activity for rich push content. Must be set in AndroidManifest.xml
        R1Emitter.getInstance().setRichPushActivity(R1RichPushActivity.class);
        // listener for receive commends
        R1Emitter.getInstance().setRichPushCommandListener(this);

        // start R1Connect
        R1Emitter.getInstance().connect(this);
    }

    @Override
    public void onCommand(WebView webView, R1Command command) {
        if ( command != null  ){
            Toast.makeText(this, "Command: " + command.getCommandName() , Toast.LENGTH_SHORT).show();
            HashMap<String,String> parameters = command.getParameters();
            for ( String key : parameters.keySet()){
                Log.i("{R1Emitter}", "Key: " + key + " Value: " + parameters.get(key));
            }
        } else {
            Toast.makeText(this, "Command: null" , Toast.LENGTH_SHORT).show();
        }
    }
}
