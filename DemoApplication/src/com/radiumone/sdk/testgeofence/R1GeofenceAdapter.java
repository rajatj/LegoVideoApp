package com.radiumone.sdk.testgeofence;

import java.util.ArrayList;

import com.radiumone.geofence_sdk.geofence.R1Geofence;
import com.radiumone.sdk.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class R1GeofenceAdapter extends BaseAdapter{

	private Context mContext;
	private ArrayList<R1Geofence> mGeofenceList;
	private ViewHolder holder;
	private LayoutInflater mInflater;
	
	private static class ViewHolder{
		TextView id;
		TextView name;
	}
	public R1GeofenceAdapter(Context context, ArrayList<R1Geofence> geofenceList){
		mContext = context;
		mGeofenceList = geofenceList;
		mInflater = LayoutInflater.from(mContext);
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mGeofenceList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mGeofenceList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return mGeofenceList.indexOf(mGeofenceList.get(position));
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null){
			convertView = mInflater.inflate(R.layout.geofence_row, null);
			holder = new ViewHolder();
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		holder.id = (TextView)convertView.findViewById(R.id.geofence_text);
		holder.name = (TextView)convertView.findViewById(R.id.name);
		holder.id.setText(mGeofenceList.get(position).getId());
		holder.name.setText(mGeofenceList.get(position).getName());
		return convertView;
	}

}
