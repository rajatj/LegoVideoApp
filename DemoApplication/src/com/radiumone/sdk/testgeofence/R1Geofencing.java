package com.radiumone.sdk.testgeofence;

import java.util.ArrayList;

import com.radiumone.geofence_sdk.R1GeofenceListener;
import com.radiumone.geofence_sdk.geofence.R1Geofence;
import com.radiumone.geofence_sdk.geofence.R1GeofenceManager;
import com.radiumone.sdk.R;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

public class R1Geofencing extends Activity implements R1GeofenceListener{
	
	private ListView mGeofenceListView;
	private R1GeofenceAdapter mGeofenceAdapter;
	private ArrayList<R1Geofence> mGeofenceList;
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.r1_geofencelist);
		mGeofenceListView = (ListView)findViewById(R.id.geofence_list);
		mGeofenceList = new ArrayList<R1Geofence>();
		mGeofenceAdapter = new R1GeofenceAdapter(this,mGeofenceList);
		mGeofenceListView.setAdapter(mGeofenceAdapter);
		updateUIWithGeofences();
		
	}
	
	public void updateUIWithGeofences(){
		populateGeofences(R1GeofenceManager.getInstance().getR1GeofenceListFromMap());
		mGeofenceAdapter.notifyDataSetChanged();


	}
	
	public void populateGeofences(ArrayList<R1Geofence> geofences){
		for(R1Geofence geofence:geofences){
			if(!mGeofenceList.contains(geofences)){
				mGeofenceList.add(geofence);
			}

			mGeofenceAdapter.notifyDataSetChanged();
		}
	}
	@Override
	public void onLocationUpdateDisconnect() {
		
	}
	@Override
	public void onR1GeofenceEntry(R1Geofence geofence) {
		
	}
	@Override
	public void onR1GeofenceExit(R1Geofence geofence) {
		
	}
	@Override
	public void onR1GeofencesAdded(final ArrayList<R1Geofence> geofenceList) {
		Log.println(Log.ERROR, "", "Geofences Added with list size "+geofenceList.size());
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				populateGeofences(geofenceList);
				

			}
		});	
	}
	
	@Override
	public void onR1LocationUpdate(Location location) {
		
	}

	@Override
	public void onR1GeofencesRemoved(ArrayList<R1Geofence> geofenceList) {
				
	}

}
