##Version 2.7.0

###1. Added

#####1. Cookie Mapping Feature

You can now enable cookie mapping which causes each deep linked Push notification to redirect the user to the Mobile Web browser on the device (Chrome on Android), set a first party cookie on the RadiumOne domain and bring the user back to the Deep Link section of the App as intended by the custom url. This behavior is only seen once in the lifetime of the cookie or if the Advertising ID of the user has changed. All future deep link Push notifications will take the user straight to the deep linked section of the app.

##Version 2.6.0

###1. Added

#####1. Rich Push Inbox Feature

The SDK now includes the ability to create an inbox that will house rich pushes.  You have the ability to store, read, delete, and display the count of rich push messages in your application.  The inbox will still function when the user disbles push notifications. 

##Version 2.5.0

###1. Added

#####1. Debugging with SDK Tools on the Portal

You can now see the JSON data sent when triggered by events in your application.  You will be able to see the events in the SDK Tools section on the portal.  To enable the debugging tool, you should create a flat file titled "r1DebugDevices" with a list of advertiser IDs inside of the "assets" folder. There should be one ID per line in your file.

##Version 2.4.1

###1. Added

#####1. App List Tracking

You may now set app list tracking with the following line in r1connect.properties:

            analytics.enable_apps_list=true

App list tracking will send a list of all installed apps on the user's device upon first application launch.  By default, this property is set to true.  You MUST set it as false if you do not want application information gathered upon first launch.

##Version 2.4.0

###1. Added

#####1. Rich Push and Deep Linking support

    <service android:name="com.radiumone.emitter.richpush.R1RichPushService" />
    <activity
            android:name="com.radiumone.emitter.richpush.activity.R1RichPushActivity"
            android:configChanges="orientation|keyboardHidden|screenSize"
            android:label="@string/title_activity_rich_push"
            android:launchMode="singleTask"
            android:parentActivityName=".MainActivity"
            android:theme="@style/AppThemeCustomNoTitle"
            android:windowSoftInputMode="stateHidden">
            <meta-data
                android:name="android.support.PARENT_ACTIVITY"
                android:value=".MainActivity" />
    </activity>

##Version 2.1.0

###1. Updated

#####1. Emit Registration

#####Before:
	
api:
	
	public void emitRegistration(UserItem userItem, Map<String, Object> otherInfo);

example usage:

	UserItem userItem = new UserItem();
	userItem.userId = sha1("userId");
	userItem.userName = "userName";
	userItem.email = "user@email.net";
	userItem.streetAddress = "address";
	userItem.phone = "123456";
	userItem.zip = "111111";
	userItem.city = "City";
	userItem.state = "State"
	R1Emitter.getInstance().emitRegistration(userItem, parameters);

######Current:

api: (removed usage of UserItem because most of fields of UserItem do not uses in this method and customer may expect this fields will be available on server)
	
	public void emitRegistration(String userId, String userName, String country, String state, String city, Map<String, Object> otherInfo);

example usage:

	R1Emitter.getInstance().emitRegistration("userId", "userName", "country", "city", "state" ,parameters);
                                     


#####2. Emit Facebook connect

#####Before:

api:

	public void emitFBConnect( String userId, String userName, pList<R1SocialPermission> permissions, Map<String, Object> otherInfo);

example usage:

	
	R1Emitter.getInstance().emitFBConnect( sha1( "userId" ), "userName", permissions, parameters);
	
	

#####Current:

api:
		
	public void emitFBConnect(List<R1SocialPermission> permissions, Map<String, Object> otherInfo)
	
example usage:

	R1Emitter.getInstance().emitFBConnect( permissions, parameters );


###2. Added

#####1. Emit User Info
	
api:
	
	public void emitUserInfo(UserItem userItem, Map<String, Object> otherInfo);
	
example usage:

	UserItem userItem = new UserItem();
	userItem.userId = "userId";
	userItem.userName = "userName";
	userItem.firstName = "firstName";
	userItem.lastName = "lastName";
	userItem.email = "user@email.net";
	userItem.streetAddress = "address";
	userItem.phone = "123456";
	userItem.zip = "111111";
	userItem.city = "City";
	userItem.state = "State"
	R1Emitter.getInstance().emitUserInfo(userItem, parameters);

#####2. Advertising Enabled

api:

	advertising_enabled=FALSE  // in r1connect.properties

	
###3. Deprecated

#####1. Emit Action

Method *emitAction* deprecated.
Use *emitEvent* instead.

#####Before:

	R1Emitter.getInstance().emitAction("Button pressed", "About", 10, parameters);

#####Current:	
	
	HashMap<String, Object> params = new HashMap<String, Object>();
	params.put("label", "About");
	params.put("value", 10);

	R1Emitter.getInstance().emitEvent("Button pressed", parameters);

#####2. sessionStart

	Method *setSessionStarted* deprecated
	
	do not use 
		R1Emitter.getInstance().setSessionStarted(true); and/or R1Emitter.getInstance().setSessionStarted(false); 
	
